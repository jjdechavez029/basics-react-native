import React, { useState } from 'react';
import { StyleSheet, Text, View, Button, Image } from 'react-native';
import State from './components/State';
import Dimensions from './components/Dimension';
import TextInput from './components/TextInput';
import Buttons from './components/Buttons';

export default function App() {
  const [outputText, setOutputText] = useState('Hello World from React Native app!') ;
  const [lineTrough, setLineTrough] = useState(false);
  let pic = {
    uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
  };
  return (
    // <View style={styles.container}>
    //   <Text>{outputText}</Text>
    //   <Image source={pic} style={{width: 193, height: 110}} />
    //   <Button 
    //     title="Chnage Text" 
    //     onPress={() => console.log('triggered')} 
    //   />
    // </View>
    // <State />
    // <Dimensions />
    // <TextInput />
    <Buttons />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ddd',
    alignItems: 'center',
    justifyContent: 'center',
  },
  displayText: {
  }
});
