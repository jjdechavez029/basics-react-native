import React, { Component } from 'react';
import { View } from 'react-native';

export default class DimensionsBasics extends Component {
    render() {
        return (
            <View style={{flex: 1, flexDirection: 'column'}}>
                <View style={{flex: 1, backgroundColor: 'powderblue'}} />
                <View style={{flex: 1, backgroundColor: 'skyblue'}} />
                <View style={{flex: 1, backgroundColor: 'steelblue'}} />
            </View>
        )
    }
}