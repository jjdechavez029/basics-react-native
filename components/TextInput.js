import React from 'react';
import { View, TextInput, Text } from 'react-native';

export default class PizzaTranslator extends React.Component {
    constructor(props) {
        super(props);
        this.state = { text: '' }
    }
    render() {
        return (
            <View style={{flex: 1, justifyContent:"center", alignItems:'center'}}>
                <TextInput
                    style={{height: 40}}
                    placeholder="Type to translate"
                    onChangeText={(text) => this.setState({text})}
                    value={this.state.text}
                />
                <Text style={{padding: 10, fontSize: 42}}>
                    {this.state.text.split(' ').map((word) => word && '🍕').join(' ')}
                </Text>
            </View>
        )
    }
}